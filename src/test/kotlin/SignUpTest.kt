import com.codeborne.selenide.Selenide
import config.WebDriverConfig
import org.assertj.core.api.Assertions.assertThat
import org.testng.annotations.BeforeMethod
import org.testng.annotations.DataProvider
import org.testng.annotations.Test
import pages.Homepage
import kotlin.test.assertSame

class SignUpTest {

    companion object {
        val baseUrl: String = "https://www.reddit.com/"
        val USERNAME_LENGTH_ERROR_MSG = "Username must be between 3 and 20 characters"
        val USERNAME_EXISTS_ERROR_MSG = "That username is already taken"
        val USERNAME_EXISTS_DELETED_ERROR_MSG = "That username is already taken by a deleted account"
        val USERNAME_SPECIAL_CHARS_ERROR_MSG = "Letters, numbers, dashes, and underscores only. Please try again without symbols."
        val INVALID_PASSWORD_ERROR_MSG = "Incorrect username or password"
    }

    @BeforeMethod
    fun openHomepage() {
        WebDriverConfig.configure()
        Selenide.open(baseUrl)
    }

    @Test
    fun t01_shouldNotLogInWithInvalidPassword() {
        //given
        val username = "username"
        val password = "this should not be the password"
        val page = Homepage()

        //when
        val modal = page.openLoginModal()
        modal.enterUsername(username)
        modal.enterPassword(password)
        modal.login()
        Selenide.actions()
                .pause(2000) //pausing to incorporate username verification
                .perform()

        //then
        assertThat(modal.getErrorMessage())
                .isEqualToIgnoringWhitespace(INVALID_PASSWORD_ERROR_MSG)
    }

    @DataProvider
    fun invalidUsernames(): Array<Array<String>> {
        return arrayOf(
                arrayOf("us", USERNAME_LENGTH_ERROR_MSG),
                arrayOf("usernameusernameusername", USERNAME_LENGTH_ERROR_MSG),
                arrayOf("username", USERNAME_EXISTS_ERROR_MSG),
                arrayOf("usernameusername", USERNAME_EXISTS_DELETED_ERROR_MSG),
                arrayOf("username!@#\$", USERNAME_SPECIAL_CHARS_ERROR_MSG),
                arrayOf("uśername !@#\$", USERNAME_SPECIAL_CHARS_ERROR_MSG),
                arrayOf("\uD83E\uDD14\uD83E\uDD14\uD83E\uDD14\uD83E\uDD14", USERNAME_SPECIAL_CHARS_ERROR_MSG),
        )
    }

    @Test(dataProvider = "invalidUsernames")
    fun t02_shouldNotAcceptInvalidUsernames(
            username: String,
            expectedMessage: String
    ) {
        //given
        val email = "email@example.com"
        val page = Homepage()

        //when
        val modal = page.openSignupModal()
        modal.enterEmail(email)
        val modalStep2 = modal.nextStep()
        modalStep2.enterUsername(username)
        Selenide.actions()
                .pause(2000) //pausing to incorporate username verification
                .perform()

        //then
        assertThat(modalStep2.getErrorMessage())
                .isEqualToIgnoringWhitespace(expectedMessage)
    }
}