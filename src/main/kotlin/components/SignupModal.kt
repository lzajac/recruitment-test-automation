package components

import com.codeborne.selenide.Condition
import com.codeborne.selenide.Selenide.*
import com.codeborne.selenide.SelenideElement
import org.openqa.selenium.By

data class SignupModalStep1(
        val rootElement: SelenideElement = element(By.className("Register")),
        val emailInput: SelenideElement = rootElement.find(By.id("regEmail")),
        val continueButton: SelenideElement = rootElement.findAll(By.tagName("button"))
                .first { it.has(Condition.attribute("type", "submit")) }
) {
    fun enterEmail(email: String) {
        emailInput.value = email
    }

    fun nextStep(): SignupModalStep2 {
        continueButton.click()
        return SignupModalStep2(rootElement)
    }
}

data class SignupModalStep2(
        val rootElement: SelenideElement = element(By.className("Register")),
        val usernameInput: SelenideElement = rootElement.find(By.id("regUsername")),
        val passwordInput: SelenideElement = rootElement.find(By.id("regPassword")),
        val signupButton: SelenideElement = rootElement.findAll(By.tagName("button"))
                .first { it.has(Condition.attribute("type", "submit")) }
) {
    fun enterUsername(username: String) {
        usernameInput.value = username
    }

    fun getErrorMessage(): String {
        return usernameInput.parent().find(By.className("AnimatedForm__errorMessage")).text()
    }

    fun nextStep() {
        TODO()
    }
}