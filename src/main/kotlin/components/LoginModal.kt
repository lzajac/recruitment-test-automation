package components

import com.codeborne.selenide.Condition
import com.codeborne.selenide.Selenide.*
import com.codeborne.selenide.SelenideElement
import org.openqa.selenium.By

data class LoginModal(
        val rootElement: SelenideElement = element(By.className("Login")),
        val usernameInput: SelenideElement = rootElement.find(By.id("loginUsername")),
        val passwordInput: SelenideElement = rootElement.find(By.id("loginPassword")),
        val loginButton: SelenideElement = rootElement.findAll(By.tagName("button"))
                .first { it.has(Condition.attribute("type", "submit")) }
) {
    fun enterUsername(username: String) {
        usernameInput.value = username
    }

    fun enterPassword(password: String) {
        passwordInput.value = password
    }

    fun login() {
        loginButton.click()
    }

    fun getErrorMessage(): String {
        return usernameInput.parent().find(By.className("AnimatedForm__errorMessage")).text()
    }
}