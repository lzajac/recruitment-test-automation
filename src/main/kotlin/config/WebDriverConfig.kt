package config

import com.codeborne.selenide.Configuration
import org.openqa.selenium.chrome.ChromeOptions
import org.openqa.selenium.remote.DesiredCapabilities


object WebDriverConfig {
    fun configure() {
        //Notification popup workaround from https://www.browserstack.com/docs/automate/selenium/handle-permission-pop-ups#handle-use-your-camera-and-use-your-microphone-pop-up
        val caps = DesiredCapabilities()
        val options = ChromeOptions()
        val prefs: MutableMap<String, Any> = HashMap()
        val profile: MutableMap<String, Any> = HashMap()
        val contentSettings: MutableMap<String, Any> = HashMap()

        contentSettings["notifications"] = 1
        profile["managed_default_content_settings"] = contentSettings
        prefs["profile"] = profile
        options.setExperimentalOption("prefs", prefs)
        caps.setCapability(ChromeOptions.CAPABILITY, options);

        Configuration.browserCapabilities = caps
    }
}