package pages

import com.codeborne.selenide.Selenide
import com.codeborne.selenide.Selenide.`$`
import com.codeborne.selenide.Selenide.element
import com.codeborne.selenide.SelenideElement
import components.LoginModal
import components.SignupModalStep1
import org.openqa.selenium.By

data class Homepage(
        val header: SelenideElement = element("header"),
        val loginButton: SelenideElement = header.findAll("a").first { it.text() == "Log In" },
        val signupButton: SelenideElement = header.findAll("a").first { it.text() == "Sign Up" }
) {
    fun openSignupModal(): SignupModalStep1 {
        signupButton.click()
        waitAndSwitchToModal()
        return SignupModalStep1()
    }

    fun openLoginModal(): LoginModal {
        loginButton.click()
        waitAndSwitchToModal()
        return LoginModal()
    }

    private fun waitAndSwitchToModal() {
        Selenide.actions()
                .pause(1000)
                .perform()
        Selenide.switchTo().frame(`$`(By.tagName("iframe")))
    }
}