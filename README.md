# Recruitment test automation

## Technologies used

- Kotlin (Open JDK 16)
- TestNG
- Selenide/Selenium
- AssertJ

## Running the tests

`mvn surefire:test`
